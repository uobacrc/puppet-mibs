# == Class: mibs
#
# Full description of class mibs here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'mibs':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class mibs {


  file { '/usr/share/snmp/mibs/PowerNet-MIB.txt':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => 0644,
    source => 'puppet:///modules/mibs/powernet417.mib',
  }


  file { '/usr/share/snmp/mibs/NETBOTZ410-MIB.txt':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => 0644,
    source => 'puppet:///modules/mibs/netbotz410.mib',
  }

  file { '/usr/share/snmp/mibs/NETBOTZ320-MIB.txt':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => 0644,
    source => 'puppet:///modules/mibs/netbotz320.mib',
  }


}
